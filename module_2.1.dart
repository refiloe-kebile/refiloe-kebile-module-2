void main() {
  var module2 = Module2(
      yourName: 'Refiloe Kebile', favoriteApp: 'Telegram', city: 'Vereeniging');

  module2.printData();
}

class Module2 {
  String yourName = '';
  String favoriteApp = '';
  String city = '';

  Module2({required yourName, required favoriteApp, required city})
      : this.yourName = yourName,
        this.favoriteApp = favoriteApp,
        this.city = city;

  void printData() {
    print('My name is ${this.yourName}');
    print('My favorite app is ${this.favoriteApp}');
    print('My current city is ${this.city}');
  }
}
