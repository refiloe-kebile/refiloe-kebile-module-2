import 'package:collection/collection.dart';

class AppOfTheYear {
  String name;
  int age;
  String catagory;
  String developer;
  AppOfTheYear(this.name, this.age, this.catagory, this.developer);
  @override
  String toString() {
    return '{Name of winning App: ${this.name.toUpperCase()}\n, Winning Year: ${this.age}\n, Catagory won: ${this.catagory}\n, App developer: ${this.developer}\n }';
  }
}

main() {
  var apps = [];
  apps.add(AppOfTheYear(
      'Ambani Africa',
      2021,
      'Best APP of the Yeat, Best gaming Solution, Best Educational and Best South African Solution',
      'Mukundi Lambani'));
  apps.add(AppOfTheYear(
      'EasyEquities',
      2020,
      'Best APP of the Year and Best Consumer Solution',
      'First World Trader (PTY)Ltd'));
  apps.add(AppOfTheYear('Naked Insurance', 2019,
      'Best Financial Solution and App Of The Year', 'Ernest North'));

  print('-----MTN WINNING APPS-----');
  print('');
  apps.sort((a, b) => a.name.compareTo(b.name));
  //name.toUppercase();
  print(apps
      .toString()
      .replaceAll(',', '')
      .replaceAll('[', '')
      .replaceAll(']', '')
      .replaceAll('{', '')
      .replaceAll('}', ''));
}
