class AppOfTheYear {
  String name;
  int age;
  AppOfTheYear(this.name, this.age);
  @override
  String toString() {
    return '{${this.name}, ${this.age} }\n';
  }
}

main() {
  //var apps = apps2021;
  var apps2021 = [];
  var apps2020 = [];
  var apps2019 = [];
  var apps2018 = [];
  var apps2017 = [];
  var apps2016 = [];
  var apps2015 = [];
  var apps2014 = [];
  var apps2013 = [];
  var apps2012 = [];

  apps2021.add(AppOfTheYear('iiDENTIFii', 2021));
  apps2021.add(AppOfTheYear('Hellopay SoftPOS', 2021));
  apps2021.add(AppOfTheYear('Guardian Health Platform', 2021));
  apps2021.add(AppOfTheYear('Ambani Africa', 2021));
  apps2021.add(AppOfTheYear('Murimi', 2021));
  apps2017.add(AppOfTheYear('Shyft', 2017));
  apps2021.add(AppOfTheYear('Shyft', 2021));
  apps2021.add(AppOfTheYear('Sisa', 2021));
  apps2021.add(AppOfTheYear('UniWise', 2021));
  apps2021.add(AppOfTheYear('Kazi', 2021));
  apps2021.add(AppOfTheYear('Takealot', 2021));
  apps2021.add(AppOfTheYear('Rekindle Learning app', 2021));
  apps2021.add(AppOfTheYear('Roadsave', 2021));
  apps2021.add(AppOfTheYear('Afrihost', 2021));
  apps2020.add(AppOfTheYear('EasyEquities', 2020));
  apps2020.add(AppOfTheYear('Examsta', 2020));
  apps2020.add(AppOfTheYear('Checkers Sixty60', 2020));
  apps2020.add(AppOfTheYear('Technishen', 2020));
  apps2020.add(AppOfTheYear('BirdPro', 2020));
  apps2020.add(AppOfTheYear('Lexie Hearing', 2020));
  apps2020.add(AppOfTheYear('League of Legends', 2020));
  apps2020.add(AppOfTheYear('GreenFingers Mobile', 2020));
  apps2020.add(AppOfTheYear('Xitsonga Dictionary', 2020));
  apps2018.add(AppOfTheYear('StokFella', 2018));
  apps2020.add(AppOfTheYear('StokFella', 2020));
  apps2020.add(AppOfTheYear('Bottles', 2020));
  apps2020.add(AppOfTheYear('Matric Live', 2020));
  apps2020.add(AppOfTheYear('Guardian Health', 2020));
  apps2020.add(AppOfTheYear('My Pregnancy Journey', 2020));
  apps2019.add(AppOfTheYear('Naked', 2019));
  apps2019.add(AppOfTheYear('SI Realities', 2019));
  apps2019.add(AppOfTheYear('Lost Defence', 2019));
  apps2019.add(AppOfTheYear('Franc', 2019));
  apps2015.add(AppOfTheYear('Vula Mobile', 2015));
  apps2019.add(AppOfTheYear('Vula Mobile', 2019));
  apps2019.add(AppOfTheYear('Matric Live', 2019));
  apps2019.add(AppOfTheYear('My Pregnancy Journal', 2019));
  apps2019.add(AppOfTheYear('Hydra', 2019));
  apps2019.add(AppOfTheYear('LocTransie', 2019));
  apps2019.add(AppOfTheYear('Over', 2019));
  apps2019.add(AppOfTheYear('Digger', 2019));
  apps2019.add(AppOfTheYear('Mo Wash', 2019));
  apps2018.add(AppOfTheYear('Pineapple', 2018));
  apps2018.add(AppOfTheYear('Cowa Bunga', 2018));
  apps2018.add(AppOfTheYear('Digemy Knowledge Partner and Besmarter', 2018));
  apps2018.add(AppOfTheYear('Bestee', 2018));
  apps2018
      .add(AppOfTheYear('The African Cyber Gaming League App (ACGL)', 2018));
  apps2018.add(AppOfTheYear('dbTrack', 2018));
  apps2018.add(AppOfTheYear('Difela Hymns', 2018));
  apps2018.add(AppOfTheYear('Xander English 1-20', 2018));
  apps2018.add(AppOfTheYear('Ctrl', 2018));
  apps2018.add(AppOfTheYear('Khula', 2018));
  apps2018.add(AppOfTheYear('ASI Snakes', 2018));
  apps2017.add(AppOfTheYear('TransUnion 1Check', 2017));
  apps2017.add(AppOfTheYear('OrderIN', 2017));
  apps2017.add(AppOfTheYear('EcoSlips', 2017));
  apps2017.add(AppOfTheYear('InterGreatMe', 2017));
  apps2017.add(AppOfTheYear('Zulzi', 2017));
  apps2017.add(AppOfTheYear('Hey Jude', 2017));
  apps2017.add(AppOfTheYear('The ORU Social', 2017));
  apps2017.add(AppOfTheYear('TouchSA', 2017));
  apps2017.add(AppOfTheYear('Pick n Pays Super Animals 2', 2017));
  apps2017.add(AppOfTheYear('The TreeApp South Africa', 2017));
  apps2017.add(AppOfTheYear('WatIf Health Portal', 2017));
  apps2017.add(AppOfTheYear('Awethu Project', 2017));
  apps2016.add(AppOfTheYear('Domestly', 2016));
  apps2016.add(AppOfTheYear('iKhoha', 2016));
  apps2016.add(AppOfTheYear('HearZA', 2016));
  apps2016.add(AppOfTheYear('Tuta-me', 2016));
  apps2016.add(AppOfTheYear('KaChing', 2016));
  apps2015.add(AppOfTheYear('Friendly Math Monsters for Kindergarten', 2016));
  apps2015.add(AppOfTheYear('DStv Now', 2015));
  apps2015.add(AppOfTheYear('WumDrop', 2015));
  apps2015.add(AppOfTheYear('CPUT Mobile', 2015));
  apps2015.add(AppOfTheYear('EskomSePush', 2015));
  apps2015.add(AppOfTheYear('M4JAM', 2015));
  apps2014.add(AppOfTheYear('SuperSport', 2014));
  apps2014.add(AppOfTheYear('SyncMobile', 2014));
  apps2014.add(AppOfTheYear('My Belongings', 2014));
  apps2014.add(AppOfTheYear('LIVE Inspect', 2014));
  apps2014.add(AppOfTheYear('Vigo', 2014));
  apps2014.add(AppOfTheYear('Zapper', 2014));
  apps2014.add(AppOfTheYear('Rea Vaya', 2014));
  apps2014.add(AppOfTheYear('Wildlife tracker', 2014));
  apps2013.add(AppOfTheYear('DSTv', 2013));
  apps2013.add(AppOfTheYear('comm Telco Data Visualizer', 2013));
  apps2013.add(AppOfTheYear('PriceCheck Mobile', 2013));
  apps2013.add(AppOfTheYear('MarkitShare', 2013));
  apps2013.add(AppOfTheYear('Nedbank App Suite', 2013));
  apps2013.add(AppOfTheYear('SnapScan', 2013));
  apps2013.add(AppOfTheYear('Kids Aid', 2013));
  apps2013.add(AppOfTheYear('bookly', 2013));
  apps2013.add(AppOfTheYear('Gautrain Buddy', 2013));
  apps2012.add(AppOfTheYear('FNB Banking App', 2012));
  apps2012.add(AppOfTheYear('HealthID (from Discovery)', 2012));
  apps2012.add(AppOfTheYear('TransUnion Dealers Guide', 2012));
  apps2012.add(AppOfTheYear('RapidTargets', 2012));
  apps2012.add(AppOfTheYear('Matchy', 2012));
  apps2012.add(AppOfTheYear('Plascon Inspire Me', 2012));
  apps2012.add(AppOfTheYear('phraZapp', 2012));

  print('-----MTN WINNING APPS FOR 2012 - 2021-----');
  apps2021.sort((a, b) => a.name.compareTo(b.name));
  apps2020.sort((a, b) => a.name.compareTo(b.name));
  apps2019.sort((a, b) => a.name.compareTo(b.name));
  apps2018.sort((a, b) => a.name.compareTo(b.name));
  apps2017.sort((a, b) => a.name.compareTo(b.name));
  apps2016.sort((a, b) => a.name.compareTo(b.name));
  apps2015.sort((a, b) => a.name.compareTo(b.name));
  apps2014.sort((a, b) => a.name.compareTo(b.name));
  apps2013.sort((a, b) => a.name.compareTo(b.name));
  apps2012.sort((a, b) => a.name.compareTo(b.name));
  //print({apps2021, apps2020});

  print({
    apps2012,
    apps2013,
    apps2014,
    apps2015,
    apps2016,
    apps2017,
    apps2018,
    apps2019,
    apps2020,
    apps2021
  }
      .toString()
      .replaceAll(',', '')
      .replaceAll('[', '')
      .replaceAll(']', '')
      .replaceAll('{', '')
      .replaceAll('}', ''));
  print('-----WINNING APPS FOR 2017 AND 2018-----');
  apps2017.sort((a, b) => a.name.compareTo(b.name));
  apps2018.sort((a, b) => a.name.compareTo(b.name));
  print({apps2017, apps2018}
      .toString()
      .replaceAll(',', '')
      .replaceAll('[', '')
      .replaceAll(']', '')
      .replaceAll('{', '')
      .replaceAll('}', ''));
  print('-----TOTAL NUMBER OF APPS-----');
  print(apps2021.length +
      apps2020.length +
      apps2019.length +
      apps2018.length +
      apps2017.length +
      apps2016.length +
      apps2015.length +
      apps2014.length +
      apps2013.length +
      apps2012.length);
}
